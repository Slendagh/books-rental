﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TP_3.Models;

namespace TP_3.Controllers
{
    public class membresController : Controller
    {
        private bdgplccEntities db = new bdgplccEntities();

        // GET: membres
        public ActionResult Index()
        {
            string messSupp = TempData["MessSupp"] as string;
            ViewBag.messSupp = messSupp;

            string messModif = TempData["messModif"] as string;
            ViewBag.messModif = messModif;
            return View(db.membres.ToList());
        }

        // GET: membres/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            membre membre = db.membres.Find(id);
            if (membre == null)
            {
                return HttpNotFound();
            }
            return View(membre);
        }

        // GET: membres/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: membres/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,prenom,nom,nomUtilisateur,email,telephone,mdp")] membre membre)
        {
            if (ModelState.IsValid)
            {
                HttpContext.Session["username"] = membre.nomUtilisateur;
                db.membres.Add(membre);
                db.SaveChanges();
                return RedirectToAction("Index", "livres", new { categ = "all" });
            }

            return View(membre);
        }

        // GET: membres/Edit/
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            membre membre = db.membres.Find(id);
            if (membre == null)
            {
                return HttpNotFound();
            }
            return View(membre);
        }

        // POST: membres/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,prenom,nom,nomUtilisateur,email,telephone,mdp")] membre membre)
        {
            if (ModelState.IsValid)
            {
                db.Entry(membre).State = EntityState.Modified;
                db.SaveChanges();
                TempData["messModif"] = "Vous venez de modifier les informations de " + membre.nomUtilisateur;
                return RedirectToAction("Index");
            }
            return View(membre);
        }

        // GET: membres/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            membre membre = db.membres.Find(id);
            if (membre == null)
            {
                return HttpNotFound();
            }
            return View(membre);
        }

        // POST: membres/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            membre membre = db.membres.Find(id);
            db.membres.Remove(membre);
            db.SaveChanges();
            TempData["MessSupp"] = "Vous venez de supprimer le membre " + membre.nomUtilisateur + ".";
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
