﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TP_3.Models;

namespace TP_3.Controllers
{
    public class livresController : Controller
    {
        private bdgplccEntities db = new bdgplccEntities();

        // GET: livres
        public ActionResult Index(string categ)
        {
            var emprunter = TempData["emprunter"] as string;
            ViewBag.emprunter = emprunter;
            var limite = TempData["limite"] as string;
            ViewBag.limite = limite;

            var dejaEmprunter = TempData["dejaEmprunter"] as string;
            ViewBag.dejaEmprunter = dejaEmprunter;

            if (categ == "all" || categ == null)
            {
                return View(db.livres.ToList());
            }
            else
            {
                var livres = db.livres.Where(l => l.categorie == categ);
                return View(livres.ToList());
            }
            
        }
        public ActionResult IndexList()
        {
            string messModif = TempData["MessModif"] as string;
            ViewBag.MessModif = messModif;

            string MessSupp = TempData["MessSupp"] as string;
            ViewBag.MessSupp = MessSupp;

            return View(db.livres.ToList());
        }

        // GET: livres/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            livre livre = db.livres.Find(id);
            if (livre == null)
            {
                return HttpNotFound();
            }
            return View(livre);
        }

        // GET: livres/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: livres/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ISBN,nom,auteur,categorie,numImg")] livre livre, HttpPostedFileBase fichier )
        {
            
            if (ModelState.IsValid)
            {
                if(fichier != null && fichier.ContentLength > 0)
                {
                    string path = Path.Combine(Server.MapPath("~/Content/images"), Path.GetFileName(fichier.FileName));
                    fichier.SaveAs(path);
                    livre.numImg = "/Content/images/" + fichier.FileName;
                }
                db.livres.Add(livre);
                db.SaveChanges();
                TempData["MessAjoutLivre"] = "Vous venez d'ajouter le livre " + livre.nom + ".";
                return RedirectToAction("Admin", "Home");
            }

            return View(livre);
        }

        // GET: livres/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            livre livre = db.livres.Find(id);
            if (livre == null)
            {
                return HttpNotFound();
            }
            return View(livre);
        }

        // POST: livres/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ISBN,nom,auteur,categorie,numImg")] livre livre)
        {
            if (ModelState.IsValid)
            {
                db.Entry(livre).State = EntityState.Modified;
                db.SaveChanges();
                TempData["MessModif"] = "Vous venez de modifier les informations du livre: " + livre.nom + ".";
                return RedirectToAction("IndexList", "livres");
            }
            return View(livre);
        }

        // GET: livres/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            livre livre = db.livres.Find(id);
            if (livre == null)
            {
                return HttpNotFound();
            }
            return View(livre);
        }

        // POST: livres/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            livre livre = db.livres.Find(id);
            db.livres.Remove(livre);
            db.SaveChanges();
            TempData["MessSupp"] = "Vous venez de supprimer le livre: " + livre.nom + ".";
            return RedirectToAction("IndexList", "livres");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
