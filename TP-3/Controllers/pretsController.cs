﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TP_3.Models;

namespace TP_3.Controllers
{
    public class pretsController : Controller
    {
        private bdgplccEntities db = new bdgplccEntities();

        // lister les prets
        public ActionResult Index(string username)
        {
            string nomUti = Session["username"] as string;
            var tab= new List<pret> { };
            if (nomUti == username)
            {
                var lesPrets = db.prets.Where(p => p.nomUtilisateur == username).ToList();
                foreach (var pret in lesPrets)
                {
                    tab.Add(pret);
                }
                var ModifSucc = TempData["ModifSucc"] as string;
                ViewBag.ModifSucc = ModifSucc;

                var retourSucc = TempData["retourSucc"] as string;
                ViewBag.RetourSucc = retourSucc;
                return View(tab.ToList());

            }
            else
                return View();
        }

        public ActionResult IndexGestion()
        {
            return View(db.prets.ToList());
        }

        // GET: prets/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            pret pret = db.prets.Find(id);
            if (pret == null)
            {
                return HttpNotFound();
            }
            return View(pret);
        }

        
        //ajouter un pret
        // POST: prets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        public ActionResult Create(int id)
        {
            int compteur = 0;
            bool dejaEmprunter = false;
            int isbn = 0;
            string user = null;
            string username = HttpContext.Session["username"] as string;
            string datePret = DateTime.Now.ToString("yy/MM/dd");
            string dateRetour = DateTime.Now.AddDays(7).ToString("yy/MM/dd");
            livre unLivre = db.livres.Find(id);
            var unPret = db.prets.Where(p => p.nomUtilisateur == username);
            foreach(var pret in unPret)
            {
                
                isbn = pret.ISBN;
                user = pret.nomUtilisateur;
                if (user == username)
                {
                    ++compteur;
                }
            }
            if(unLivre != null)
            {
                
                if(isbn == unLivre.ISBN && user == username)
                {
                    dejaEmprunter = true;
                }
                if (compteur >= 3)
                {
                    TempData["limite"] = "Vous avez une limite de 3 livres à la fois";
                    return this.RedirectToAction("Index", "livres", new { categ = "all" });
                }
                else if(dejaEmprunter)
                {
                    TempData["dejaEmprunter"] = "Vous avez déjà emprunter ce livre.";
                    return this.RedirectToAction("Index", "livres", new { categ = "all" });
                }
                else{
                    pret nouvPret = new pret
                    {
                        nomUtilisateur = username,
                        ISBN = unLivre.ISBN,
                        nom = unLivre.nom,
                        auteur = unLivre.auteur,
                        categories = unLivre.categorie,
                        datePret = datePret,
                        dateRetour = dateRetour

                    };
                    db.prets.Add(nouvPret);
                    db.SaveChanges();
                    TempData["emprunter"] = "Vous venez d'emprunter " + unLivre.nom + ", Bonne lecture !";
                    return this.RedirectToAction("Index", "livres", new { categ = "all" });
                }
            }
            return RedirectToAction("Index");
        }

        // GET: prets/Create
       /* public ActionResult Create()
        {
            return View();
        }*/

        // GET: prets/Edit/5
        public ActionResult Edit(int? id)
        {
            DateTime dr;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            pret pret = db.prets.Find(id);
            if (pret == null)
            {
                return HttpNotFound();
            }
            else
            {
                string dateRetour = pret.dateRetour;
                DateTime.TryParseExact(dateRetour, "yy/MM/dd", System.Globalization.CultureInfo.GetCultureInfo("fr-FR"), System.Globalization.DateTimeStyles.None, out dr);
                pret.dateRetour = dr.AddDays(7).ToString("yy/MM/dd");
                db.Entry(pret).State = EntityState.Modified;
                db.SaveChanges();

                TempData["ModifSucc"] = "Vous venez de prolonger la date de retour de 7 jours.";
                return RedirectToAction("Index", "prets", new { username = Session["username"] });
            }
        }

       /* // POST: prets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,nomUtilisateur,ISBN,nom,categories,auteur,datePret,dateRetour")] pret pret)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pret).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pret);
        } */

        // GET: prets/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            pret pret = db.prets.Find(id);
            if (pret == null)
            {
                return HttpNotFound();
            }
            return View(pret);
        }

        // POST: prets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            pret pret = db.prets.Find(id);
            db.prets.Remove(pret);
            db.SaveChanges();
            //supprimer des retards
            string nom = pret.nom;
            var lesretard = db.retards.Where(r => r.nom == nom).ToList();
            bool vrai = false;
            retard unretard = new retard();
            foreach(var unretar in lesretard)
            {
                if(unretar.nomUtilisateur == pret.nomUtilisateur)
                {
                    vrai = true;
                    unretard = unretar;
                }
            }
            if (vrai)
            {
                db.retards.Remove(unretard);
                db.SaveChanges();
            }
            TempData["retourSucc"] = "Vous venez de retourner le livre: " + pret.nom + ".";
            return this.RedirectToAction("Index", "prets", new { username = Session["username"] });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
