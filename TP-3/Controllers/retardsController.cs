﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TP_3.Models;

namespace TP_3.Controllers
{
    public class retardsController : Controller
    {
        private bdgplccEntities db = new bdgplccEntities();

        // GET: retards
        public ActionResult Index()
        {
            return View(db.retards.ToList());
        }

        // GET: retards/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            retard retard = db.retards.Find(id);
            if (retard == null)
            {
                return HttpNotFound();
            }
            return View(retard);
        }

        // GET: retards/Create
        public ActionResult Create()
        {
            var listePret = db.prets.ToList();
            var retards = db.retards.ToList();
            string dateRetour;
            DateTime dr;
            retard nouvRetard;
            var tab = new List<retard> { };
            bool existeDeja = false;

            foreach (var pret in listePret)
            {
                //verification sinexiste deja dans retard
                foreach (var retard in retards)
                {
                    if (retard.ISBN == pret.ISBN)
                    {
                        existeDeja = true;
                    }
                }
                dateRetour = pret.dateRetour;
                DateTime.TryParseExact(dateRetour, "yy/MM/dd", System.Globalization.CultureInfo.GetCultureInfo("fr-FR"), System.Globalization.DateTimeStyles.None, out dr);

                if (DateTime.Compare(dr, DateTime.Now) < 0 && existeDeja == false)
                {
                    nouvRetard = new retard
                    {
                        nom = pret.nom,
                        nomUtilisateur = pret.nomUtilisateur,
                        ISBN = pret.ISBN,
                        categories = pret.categories,
                        auteur = pret.auteur,
                        datePret = pret.datePret,
                        dateRetour = pret.dateRetour
                    };
                    db.retards.Add(nouvRetard);
                    db.SaveChanges();
                }

            }
            return RedirectToAction("Index");
        }

        // POST: retards/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
       /* [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,nomUtilisateur,ISBN,nom,categories,auteur,datePret,dateRetour")] retard retard)
        {
            if (ModelState.IsValid)
            {
                db.retards.Add(retard);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(retard);
        }*/

        // GET: retards/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            retard retard = db.retards.Find(id);
            if (retard == null)
            {
                return HttpNotFound();
            }
            return View(retard);
        }

        // POST: retards/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,nomUtilisateur,ISBN,nom,categories,auteur,datePret,dateRetour")] retard retard)
        {
            if (ModelState.IsValid)
            {
                db.Entry(retard).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(retard);
        }

        // GET: retards/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            retard retard = db.retards.Find(id);
            if (retard == null)
            {
                return HttpNotFound();
            }
            return View(retard);
        }

        // POST: retards/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            retard retard = db.retards.Find(id);
            db.retards.Remove(retard);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
