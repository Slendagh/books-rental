﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TP_3.Models;

namespace TP_3.Controllers
{
    public class HomeController : Controller
    {
        private bdgplccEntities db = new bdgplccEntities();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {
            string username=null, mdp=null;
            string nomUtilisateur = collection["nomUtilisateur"];
            var dejaMembre = db.membres.Where(m => m.nomUtilisateur == nomUtilisateur).ToList();
            foreach(var mb in dejaMembre)
            {
                username = mb.nomUtilisateur;
                mdp = mb.mdp;
            }
            if(dejaMembre != null)
            {
                if (username != null && mdp == collection["mdp"])
                {
                    HttpContext.Session["username"] = collection["nomUtilisateur"];
                    return RedirectToAction("Index", "livres", new { categ = "all" });
                }
                ViewBag.Error = "Nom d'utilisateur ou mot de passe incorrect";
                return View();
            }
            return View("~/Views/Shared/Error.cshtml");

        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult SeDeconecter()
        {
            Session.Clear();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Admin()
        {
            string messAjoutLivre = TempData["MessAjoutLivre"] as string;
            ViewBag.MessAjoutLivre = messAjoutLivre;
            return View();
        }
    }
}